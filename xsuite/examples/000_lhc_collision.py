import numpy as np
import xtrack as xt

import xs_lhc_op_utils as xop

# This script reproduces in Xsuite the MAD-X script:
# 'acc-models-lhc/scenarios/pp_lumi/SQUEEZE-6.8TeV-1.2m-30cm-2023_V1/696/model.madx

collider = xt.Multiline.from_json('../lhc.json')
collider.build_trackers()
collider.lhcb1.particle_ref.p0c = 6800e9
collider.lhcb2.particle_ref.p0c = 6800e9

collider.vars.set_from_madx_file([
    '../../../acc-models-lhc/toolkit/zero-strengths.madx',
    '../../../acc-models-lhc/strengths/ATS_Nominal/2023/ats_30cm.madx',
    '../../../acc-models-lhc/toolkit/reset-bump-flags.madx'])

xop.match_lumi_kbobs(collider)
xop.generate_op_tune_knobs_ats(collider)
xop.generate_op_chroma_knobs_ats(collider)
xop.generate_op_coupling_knobs_ats(collider)
xop.generate_phasechange_knobs(collider)


collider.vars.set_from_madx_file('../../../acc-models-lhc/scenarios/pp_lumi/'
                'SQUEEZE-6.8TeV-1.2m-30cm-2023_V1/696/settings.madx')

twb1 = collider.lhcb1.twiss()
twb2 = collider.lhcb2.twiss()

import matplotlib.pyplot as plt
plt.close('all')
plt.figure(1, figsize=(6.4, 4.8 * 1.4))

ax1 = plt.subplot(4, 1, 1)
ax1.plot(twb1.s, twb1.betx, 'b', label='b1')
ax1.plot(twb2.s, twb2.betx, 'r', label='b2')
ax1.set_ylabel(r'$\beta_x$ [m]')

ax2 = plt.subplot(4, 1, 2, sharex=ax1)
ax2.plot(twb1.s, twb1.bety, 'b')
ax2.plot(twb2.s, twb2.bety, 'r')
ax2.set_ylabel(r'$\beta_y$ [m]')

ax3 = plt.subplot(4, 1, 3, sharex=ax1)
ax3.plot(twb1.s, twb1.x * 1e3, 'b')
ax3.plot(twb2.s, twb2.x * 1e3, 'r')
ax3.set_ylabel(r'$x$ [mm]')

ax4 = plt.subplot(4, 1, 4, sharex=ax1)
ax4.plot(twb1.s, twb1.y * 1e3, 'b')
ax4.plot(twb2.s, twb2.y * 1e3, 'r')
ax4.set_ylabel(r'$y$ [mm]')

plt.show()

