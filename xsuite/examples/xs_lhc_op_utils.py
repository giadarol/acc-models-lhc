import numpy as np
import xtrack as xt

LUMI_KNOBS = [
    dict(
        knob_name='on_xip1b1',
        correctors = ['acbch5.r1b1', 'acbyhs4.r1b1', 'acbch6.l1b1', 'acbyh4.l1b1'],
        x_target = 0.001,
        y_target = 0,
        ip_name='ip1',
        ele_start='e.ds.l1.b1',
        ele_stop='s.ds.r1.b1',
    ),
    dict(
        knob_name='on_xip1b2',
        correctors=['acbyh4.r1b2', 'acbch6.r1b2', 'acbyhs4.l1b2', 'acbch5.l1b2'],
        x_target = 0.001,
        y_target = 0,
        ip_name='ip1',
        ele_start='e.ds.l1.b2',
        ele_stop='s.ds.r1.b2',
    ),
    dict(
        knob_name='on_yip1b1',
        correctors=['acbyvs4.l1b1', 'acbcv5.l1b1', 'acbyv4.r1b1', 'acbcv6.r1b1'],
        x_target = 0,
        y_target = 0.001,
        ip_name='ip1',
        ele_start='e.ds.l1.b1',
        ele_stop='s.ds.r1.b1',
    ),
    dict(
        knob_name='on_yip1b2',
        correctors=['acbcv5.r1b2', 'acbyv4.l1b2', 'acbcv6.l1b2', 'acbyvs4.r1b2'],
        x_target = 0,
        y_target = 0.001,
        ip_name='ip1',
        ele_start='e.ds.l1.b2',
        ele_stop='s.ds.r1.b2',
    ),
    dict(
        knob_name='on_xip2b1',
        correctors=['acbchs5.r2b1', 'acbyhs4.l2b1', 'acbyh5.l2b1', 'acbyh4.r2b1'],
        x_target = 0.001,
        y_target = 0,
        ip_name='ip2',
        ele_start='e.ds.l2.b1',
        ele_stop='s.ds.r2.b1',
    ),
    dict(
        knob_name='on_xip2b2',
        correctors=['acbyh4.l2b2', 'acbch5.r2b2', 'acbyhs4.r2b2', 'acbyhs5.l2b2'],
        x_target = 0.001,
        y_target = 0,
        ip_name='ip2',
        ele_start='e.ds.l2.b2',
        ele_stop='s.ds.r2.b2',
    ),
    dict(
        knob_name='on_yip2b1',
        correctors=['acbcv5.r2b1', 'acbyv4.l2b1', 'acbyvs5.l2b1', 'acbyvs4.r2b1'],
        x_target = 0,
        y_target = 0.001,
        ip_name='ip2',
        ele_start='e.ds.l2.b1',
        ele_stop='s.ds.r2.b1',
    ),
    dict(
        knob_name='on_yip2b2',
        correctors=['acbcvs5.r2b2', 'acbyv4.r2b2', 'acbyv5.l2b2', 'acbyvs4.l2b2'],
        x_target = 0,
        y_target = 0.001,
        ip_name='ip2',
        ele_start='e.ds.l2.b2',
        ele_stop='s.ds.r2.b2',
    ),
    dict(
        knob_name='on_xip5b1',
        correctors=['acbch6.l5b1', 'acbyhs4.r5b1', 'acbyh4.l5b1', 'acbch5.r5b1'],
        x_target = 0.001,
        y_target = 0,
        ip_name='ip5',
        ele_start='e.ds.l5.b1',
        ele_stop='s.ds.r5.b1',
    ),
    dict(
        knob_name='on_xip5b2',
        correctors=['acbch6.r5b2', 'acbyh4.r5b2', 'acbch5.l5b2', 'acbyhs4.l5b2'],
        x_target = 0.001,
        y_target = 0,
        ip_name='ip5',
        ele_start='e.ds.l5.b2',
        ele_stop='s.ds.r5.b2',
    ),
    dict(
        knob_name='on_yip5b1',
        correctors=['acbyvs4.l5b1', 'acbcv6.r5b1', 'acbyv4.r5b1', 'acbcv5.l5b1'],
        x_target = 0,
        y_target = 0.001,
        ip_name='ip5',
        ele_start='e.ds.l5.b1',
        ele_stop='s.ds.r5.b1',
    ),
    dict(
        knob_name='on_yip5b2',
        correctors=['acbyvs4.r5b2', 'acbcv5.r5b2', 'acbcv6.l5b2', 'acbyv4.l5b2'],
        x_target = 0,
        y_target = 0.001,
        ip_name='ip5',
        ele_start='e.ds.l5.b2',
        ele_stop='s.ds.r5.b2',
    ),
    dict(
        knob_name='on_xip8b1',
        correctors=['acbyhs4.l8b1', 'acbch5.l8b1', 'acbch6.r8b1', 'acbyh4.r8b1'],
        x_target = 0.001,
        y_target = 0,
        ip_name='ip8',
        ele_start='e.ds.l8.b1',
        ele_stop='s.ds.r8.b1',
    ),
    dict(
        knob_name='on_xip8b2',
        correctors=['acbyh4.l8b2', 'acbyhs4.r8b2', 'acbyh5.r8b2', 'acbchs5.l8b2'],
        x_target = 0.001,
        y_target = 0,
        ip_name='ip8',
        ele_start='e.ds.l8.b2',
        ele_stop='s.ds.r8.b2',
    ),
    dict(
        knob_name='on_yip8b1',
        correctors=['acbyv4.l8b1', 'acbyvs4.r8b1', 'acbcvs5.l8b1', 'acbyv5.r8b1'],
        x_target = 0,
        y_target = 0.001,
        ip_name='ip8',
        ele_start='e.ds.l8.b1',
        ele_stop='s.ds.r8.b1',
    ),
    dict(
        knob_name='on_yip8b2',
        correctors=['acbyvs5.r8b2', 'acbyvs4.l8b2', 'acbyv4.r8b2', 'acbcv5.l8b2'],
        x_target = 0,
        y_target = 0.001,
        ip_name='ip8',
        ele_start='e.ds.l8.b2',
        ele_stop='s.ds.r8.b2',
    ),
]

def match_single_baem_ip_knobs(collider, knobs_to_match):

    opt_knobs = {}
    for knob in knobs_to_match:
        knob_name = knob['knob_name']
        x_target = knob['x_target']
        y_target = knob['y_target']
        correctors = knob['correctors']
        ele_start = knob['ele_start']
        ele_stop = knob['ele_stop']
        ip_name = knob['ip_name']

        line_name = 'lhc' + knob_name[-2:]
        assert line_name in ['lhcb1', 'lhcb2']

        opt = collider[line_name].match_knob(
            run=False,
            knob_name=knob_name,
            ele_start=ele_start, ele_stop=ele_stop,
            twiss_init=xt.TwissInit(x=0, px=0, y=0, py=0),
            vary=[
                xt.VaryList(correctors),
            ],
            targets=[
                xt.TargetSet(x=x_target, y=y_target, px=0, py=0, at=ip_name),
                xt.TargetSet(x=0, px=0, y=0, py=0, at=xt.END),
            ])
        collider.vv[opt.vary[0].name] = 1e-7
        opt.add_point_to_log()

        opt.solve()

        opt.generate_knob()
        opt_knobs[knob_name] = opt

def match_lumi_kbobs(collider):
    match_single_baem_ip_knobs(collider, LUMI_KNOBS)

def copy_knob_by_probing(collider, varname, varname_new, controlled_vars):
    collider.vv[varname] = 0
    var_values_0 = {kk: collider.vv[kk] for kk in controlled_vars}
    collider.vv[varname] = 1
    var_values_1 = {kk: collider.vv[kk] for kk in controlled_vars}
    collider.vv[varname] = 0

    collider.vv[varname_new] = 0
    for kk in controlled_vars:
        collider.vars[kk] += collider.vars[varname_new] * (var_values_1[kk] - var_values_0[kk])

def generate_op_tune_knobs_ats(collider):
    controlled_vars_b1 = (
        'kqtf.a12b1', 'kqtf.a23b1', 'kqtf.a34b1', 'kqtf.a45b1',
        'kqtf.a56b1', 'kqtf.a67b1', 'kqtf.a78b1', 'kqtf.a81b1',
        'kqtd.a12b1', 'kqtd.a23b1', 'kqtd.a34b1', 'kqtd.a45b1',
        'kqtd.a56b1', 'kqtd.a67b1', 'kqtd.a78b1', 'kqtd.a81b1',
        )
    copy_knob_by_probing(collider, 'dqx.b1_sq', 'dqx.b1_op', controlled_vars_b1)
    copy_knob_by_probing(collider, 'dqy.b1_sq', 'dqy.b1_op', controlled_vars_b1)

    controlled_vars_b2 = (
        'kqtf.a12b2', 'kqtf.a23b2', 'kqtf.a34b2', 'kqtf.a45b2',
        'kqtf.a56b2', 'kqtf.a67b2', 'kqtf.a78b2', 'kqtf.a81b2',
        'kqtd.a12b2', 'kqtd.a23b2', 'kqtd.a34b2', 'kqtd.a45b2',
        'kqtd.a56b2', 'kqtd.a67b2', 'kqtd.a78b2', 'kqtd.a81b2',
        )
    copy_knob_by_probing(collider, 'dqx.b2_sq', 'dqx.b2_op', controlled_vars_b2)
    copy_knob_by_probing(collider, 'dqy.b2_sq', 'dqy.b2_op', controlled_vars_b2)

def generate_op_chroma_knobs_ats(collider):
    controlled_vars_b1 = (
        'ksd1.a12b1', 'ksd1.a23b1', 'ksd1.a34b1', 'ksd1.a45b1',
        'ksd1.a56b1', 'ksd1.a67b1', 'ksd1.a78b1', 'ksd1.a81b1',
        'ksd2.a12b1', 'ksd2.a23b1', 'ksd2.a34b1', 'ksd2.a45b1',
        'ksd2.a56b1', 'ksd2.a67b1', 'ksd2.a78b1', 'ksd2.a81b1',
        'ksf1.a12b1', 'ksf1.a23b1', 'ksf1.a34b1', 'ksf1.a45b1',
        'ksf1.a56b1', 'ksf1.a67b1', 'ksf1.a78b1', 'ksf1.a81b1',
        'ksf2.a12b1', 'ksf2.a23b1', 'ksf2.a34b1', 'ksf2.a45b1',
        'ksf2.a56b1', 'ksf2.a67b1', 'ksf2.a78b1', 'ksf2.a81b1')
    copy_knob_by_probing(collider, 'dqpx.b1_sq', 'dqpx.b1_op', controlled_vars_b1)
    copy_knob_by_probing(collider, 'dqpy.b1_sq', 'dqpy.b1_op', controlled_vars_b1)

    controlled_vars_b2 = [
        'ksd1.a12b2', 'ksd1.a23b2', 'ksd1.a34b2', 'ksd1.a45b2', 'ksd1.a56b2',
        'ksd1.a67b2', 'ksd1.a78b2', 'ksd1.a81b2', 'ksd2.a12b2', 'ksd2.a23b2',
        'ksd2.a34b2', 'ksd2.a45b2', 'ksd2.a56b2', 'ksd2.a67b2', 'ksd2.a78b2',
        'ksd2.a81b2', 'ksf1.a12b2', 'ksf1.a23b2', 'ksf1.a34b2', 'ksf1.a45b2',
        'ksf1.a56b2', 'ksf1.a67b2', 'ksf1.a78b2', 'ksf1.a81b2', 'ksf2.a12b2',
        'ksf2.a23b2', 'ksf2.a34b2', 'ksf2.a45b2', 'ksf2.a56b2', 'ksf2.a67b2',
        'ksf2.a78b2', 'ksf2.a81b2']
    copy_knob_by_probing(collider, 'dqpx.b2_sq', 'dqpx.b2_op', controlled_vars_b2)
    copy_knob_by_probing(collider, 'dqpy.b2_sq', 'dqpy.b2_op', controlled_vars_b2)

def generate_op_coupling_knobs_ats(collider):

    controlled_vars_b1 = [
        'kqs.a23b1', 'kqs.a45b1', 'kqs.a67b1', 'kqs.a81b1', 'kqs.l2b1',
        'kqs.l4b1', 'kqs.l6b1',  'kqs.l8b1',  'kqs.r1b1',  'kqs.r3b1',
        'kqs.r5b1', 'kqs.r7b1']
    copy_knob_by_probing(collider, 'cmrs.b1_sq', 'cmrs.b1_op', controlled_vars_b1)
    copy_knob_by_probing(collider, 'cmis.b1_sq', 'cmis.b1_op', controlled_vars_b1)

    controlled_vars_b2 = [
        'kqs.a12b2', 'kqs.a34b2', 'kqs.a56b2', 'kqs.a78b2', 'kqs.l1b2',
        'kqs.l3b2', 'kqs.l5b2', 'kqs.l7b2', 'kqs.r2b2', 'kqs.r4b2',
        'kqs.r6b2', 'kqs.r8b2']
    copy_knob_by_probing(collider, 'cmrs.b2_sq', 'cmrs.b2_op', controlled_vars_b2)
    copy_knob_by_probing(collider, 'cmis.b2_sq', 'cmis.b2_op', controlled_vars_b2)

def generate_phasechange_knobs(collider):
    collider.vars['phase_change.b1'] = 0
    collider.vars['kqtf.a12b1'] += -0.0022477200000 * collider.vars['phase_change.b1']
    collider.vars['kqtf.a23b1'] += -0.0006109026670 * collider.vars['phase_change.b1']
    collider.vars['kqtf.a34b1'] += -0.0006740726670 * collider.vars['phase_change.b1']
    collider.vars['kqtf.a45b1'] += +0.0015222900000 * collider.vars['phase_change.b1']
    collider.vars['kqtf.a56b1'] += +0.0011189300000 * collider.vars['phase_change.b1']
    collider.vars['kqtf.a67b1'] += +0.0020387763940 * collider.vars['phase_change.b1']
    collider.vars['kqtf.a78b1'] += -0.0011010306070 * collider.vars['phase_change.b1']
    collider.vars['kqtf.a81b1'] += -0.0001300250000 * collider.vars['phase_change.b1']
    collider.vars['kqtd.a12b1'] += -0.0001437190000 * collider.vars['phase_change.b1']
    collider.vars['kqtd.a23b1'] += +0.0010619748420 * collider.vars['phase_change.b1']
    collider.vars['kqtd.a34b1'] += +0.0001529048423 * collider.vars['phase_change.b1']
    collider.vars['kqtd.a45b1'] += -0.0004891330000 * collider.vars['phase_change.b1']
    collider.vars['kqtd.a56b1'] += +0.0008419600000 * collider.vars['phase_change.b1']
    collider.vars['kqtd.a67b1'] += +0.0016072722540 * collider.vars['phase_change.b1']
    collider.vars['kqtd.a78b1'] += -0.0013696167460 * collider.vars['phase_change.b1']
    collider.vars['kqtd.a81b1'] += -0.0016425400000 * collider.vars['phase_change.b1']

    collider.vars['phase_change.b2'] = 0
    collider.vars['kqtf.a12b2'] += -0.0015000300000 * collider.vars['phase_change.b2']
    collider.vars['kqtf.a23b2'] += -0.0026080999780 * collider.vars['phase_change.b2']
    collider.vars['kqtf.a34b2'] += +0.0002292920220 * collider.vars['phase_change.b2']
    collider.vars['kqtf.a45b2'] += +0.0018962000000 * collider.vars['phase_change.b2']
    collider.vars['kqtf.a56b2'] += +0.0027266500000 * collider.vars['phase_change.b2']
    collider.vars['kqtf.a67b2'] += -0.0005254090387 * collider.vars['phase_change.b2']
    collider.vars['kqtf.a78b2'] += -0.0006960890387 * collider.vars['phase_change.b2']
    collider.vars['kqtf.a81b2'] +=  0.0004939700000 * collider.vars['phase_change.b2']
    collider.vars['kqtd.a12b2'] += -0.0006047010000 * collider.vars['phase_change.b2']
    collider.vars['kqtd.a23b2'] += +0.0007281687569 * collider.vars['phase_change.b2']
    collider.vars['kqtd.a34b2'] += +0.0015548136570 * collider.vars['phase_change.b2']
    collider.vars['kqtd.a45b2'] += -0.0003441180000 * collider.vars['phase_change.b2']
    collider.vars['kqtd.a56b2'] += +0.0002527790000 * collider.vars['phase_change.b2']
    collider.vars['kqtd.a67b2'] += -0.0024345517550 * collider.vars['phase_change.b2']
    collider.vars['kqtd.a78b2'] += -0.0006010707552 * collider.vars['phase_change.b2']
    collider.vars['kqtd.a81b2'] += +0.0014239700000 * collider.vars['phase_change.b2']