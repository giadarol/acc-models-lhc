import numpy as np
import xtrack as xt

from  ..examples import xs_lhc_op_utils as xop

def test_op_knobs():
    collider = xt.Multiline.from_json('../lhc.json')
    collider.build_trackers()

    collider.vars.set_from_madx_file([
        '../../../acc-models-lhc/toolkit/zero-strengths.madx',
        '../../../acc-models-lhc/strengths/ATS_Nominal/2023/ats_30cm.madx',
        '../../../acc-models-lhc/toolkit/reset-bump-flags.madx'])

    xop.match_lumi_kbobs(collider)
    xop.generate_op_tune_knobs_ats(collider)
    xop.generate_op_chroma_knobs_ats(collider)
    xop.generate_op_coupling_knobs_ats(collider)
    xop.generate_phasechange_knobs(collider)

    twb1 = collider.lhcb1.twiss()
    twb2 = collider.lhcb2.twiss()

    # Check against corresponding madx model
    from cpymad.madx import Madx
    mad = Madx()
    mad.call("../../../acc-models-lhc/lhc.seq")

    mad.input(
    '''
    beam, sequence=lhcb1, bv= 1, particle=proton, charge=1, mass=0.938272046,
    pc= 6800.0,   npart=1.2e11,kbunch=2556, ex=5.2126224777777785e-09,ey=5.2126224777777785e-09;
    beam, sequence=lhcb2, bv=-1, particle=proton, charge=1, mass=0.938272046,
    pc= 6800.0,   npart=1.2e11,kbunch=2556, ex=5.2126224777777785e-09,ey=5.2126224777777785e-09;
    '''
    )


    mad.use(sequence='lhcb1')
    mad.use(sequence='lhcb2')

    mad.call("../../../acc-models-lhc/toolkit/zero-strengths.madx")
    mad.call("../../../acc-models-lhc/strengths/ATS_Nominal/2023/ats_30cm.madx")
    mad.call("../../../acc-models-lhc/toolkit/reset-bump-flags.madx")
    mad.call("../../../acc-models-lhc/toolkit/match-lumiknobs.madx")
    mad.call("../../../acc-models-lhc/toolkit/generate-op-tune-knobs-ats.madx")
    mad.call("../../../acc-models-lhc/toolkit/generate-op-chroma-knobs-ats.madx")
    mad.call("../../../acc-models-lhc/toolkit/generate-op-coupling-knobs-ats.madx")
    mad.call("../../../acc-models-lhc/toolkit/generate-phasechange-knobs.madx")

    # Check lumi knobs

    lumi_knobs_test_vals= {
        'on_xip1b1': 0.0001, 'on_xip1b2': 0.0002, 'on_yip1b1': 0.0003, 'on_yip1b2': 0.0004,
        'on_xip2b1': 0.0005, 'on_xip2b2': 0.0006, 'on_yip2b1': 0.0007, 'on_yip2b2': 0.0008,
        'on_xip5b1': 0.0009, 'on_xip5b2': 0.0010, 'on_yip5b1': 0.0011, 'on_yip5b2': 0.0012,
        'on_xip8b1': 0.0013, 'on_xip8b2': 0.0014, 'on_yip8b1': 0.0015, 'on_yip8b2': 0.0016,
    }

    for kk in lumi_knobs_test_vals.keys():
        mad.globals[kk] = lumi_knobs_test_vals[kk]
        collider.vars[kk] = lumi_knobs_test_vals[kk]

    mad.use(sequence='lhcb1')
    mad.use(sequence='lhcb2')

    twmb1 = mad.twiss(sequence='lhcb1', table='tw1')
    twmb2 = mad.twiss(sequence='lhcb2', table='tw2')
    twxb1 = collider.lhcb1.twiss()
    twxb2 = collider.lhcb2.twiss()

    assert np.allclose(np.interp(twxb1.s, twmb1.s, twmb1.x), twxb1.x, rtol=0, atol=4e-8)
    assert np.allclose(np.interp(twxb1.s, twmb1.s, twmb1.y), twxb1.y, rtol=0, atol=4e-8)
    assert np.allclose(np.interp(twxb2.s, twmb2.s, twmb2.x), twxb2.x, rtol=0, atol=4e-8)
    assert np.allclose(np.interp(twxb2.s, twmb2.s, twmb2.y), twxb2.y, rtol=0, atol=4e-8)

    for kk in lumi_knobs_test_vals.keys():
        mad.globals[kk] = 0.
        collider.vars[kk] = 0.

    # Check tune knobs
    q_test_vals = {'dqx.b1_op': 0.01, 'dqy.b1_op': 0.02,
                'dqx.b2_op': 0.03, 'dqy.b2_op': 0.04}

    for kk in q_test_vals.keys():
        mad.globals[kk] = q_test_vals[kk]
        collider.vars[kk] = q_test_vals[kk]

    twmb1 = mad.twiss(sequence='lhcb1', table='tw1')
    twmb2 = mad.twiss(sequence='lhcb2', table='tw2')
    twxb1 = collider.lhcb1.twiss(group_compound_elements=True)
    twxb2 = collider.lhcb2.twiss(group_compound_elements=True)

    assert np.allclose(np.interp(twxb1.s, twmb1.s, twmb1.mux), twxb1.mux, rtol=0, atol=4e-8)
    assert np.allclose(np.interp(twxb1.s, twmb1.s, twmb1.muy), twxb1.muy, rtol=0, atol=4e-8)
    assert np.allclose(np.interp(twxb2.s, twmb2.s, twmb2.mux), twxb2.mux, rtol=0, atol=4e-8)
    assert np.allclose(np.interp(twxb2.s, twmb2.s, twmb2.muy), twxb2.muy, rtol=0, atol=4e-8)

    for kk in q_test_vals.keys():
        mad.globals[kk] = 0
        collider.vars[kk] = 0

    # Check chroma knobs
    chroma_test_vals = {'dqpx.b1_op': 2, 'dqpy.b1_op': 3,
                        'dqpx.b2_op': 4, 'dqpy.b2_op': 5}


    for kk in chroma_test_vals.keys():
        mad.globals[kk] = chroma_test_vals[kk]
        collider.vars[kk] = chroma_test_vals[kk]

    twmb1 = mad.twiss(sequence='lhcb1', table='tw1')
    twmb2 = mad.twiss(sequence='lhcb2', table='tw2')
    twxb1 = collider.lhcb1.twiss(group_compound_elements=True)
    twxb2 = collider.lhcb2.twiss(group_compound_elements=True)

    assert np.isclose(twmb1.summary.dq1, twxb1.dqx, rtol=0, atol=0.1)
    assert np.isclose(twmb1.summary.dq2, twxb1.dqy, rtol=0, atol=0.1)
    assert np.isclose(twmb2.summary.dq1, twxb2.dqx, rtol=0, atol=0.1)
    assert np.isclose(twmb2.summary.dq2, twxb2.dqy, rtol=0, atol=0.1)

    for kk in chroma_test_vals.keys():
        mad.globals[kk] = 0
        collider.vars[kk] = 0

    # Check coupling knobs
    coupling_test_vals = {'cmrs.b1_op': 1e-3, 'cmrs.b2_op': 2e-3,
                        'cmis.b1_op': 3e-3, 'cmis.b2_op': 4e-3}

    for kk in coupling_test_vals.keys():
        mad.globals[kk] = coupling_test_vals[kk]
        collider.vars[kk] = coupling_test_vals[kk]

    mad.input('twiss, sequence=lhcb1, ripken')
    t1df = mad.table.twiss.dframe()
    mad.input('twiss, sequence=lhcb2, ripken')
    t2df = mad.table.twiss.dframe()
    twxb1 = collider.lhcb1.twiss(group_compound_elements=True)
    twxb2 = collider.lhcb2.twiss(group_compound_elements=True)

    assert np.allclose(np.interp(twxb1.s, t1df.s, t1df.beta12), twxb1.betx2,
                    rtol=0, atol=1e-3)
    assert np.allclose(np.interp(twxb1.s, t1df.s, t1df.beta21), twxb1.bety1,
                    rtol=0, atol=1e-3)
    assert np.allclose(np.interp(twxb2.s, t2df.s, t2df.beta12), twxb2.betx2,
                    rtol=0, atol=1e-3)
    assert np.allclose(np.interp(twxb2.s, t2df.s, t2df.beta21), twxb2.bety1,
                    rtol=0, atol=1e-3)

    for kk in coupling_test_vals.keys():
        mad.globals[kk] = 0
        collider.vars[kk] = 0

    # Check phase change knobs
    phasechange_test_vals = {'phase_change.b1': 0.5, 'phase_change.b2': 0.6}

    for kk in phasechange_test_vals.keys():
        mad.globals[kk] = phasechange_test_vals[kk]
        collider.vars[kk] = phasechange_test_vals[kk]

    twmb1 = mad.twiss(sequence='lhcb1', table='tw1')
    twmb2 = mad.twiss(sequence='lhcb2', table='tw2')
    twxb1 = collider.lhcb1.twiss(group_compound_elements=True)
    twxb2 = collider.lhcb2.twiss(group_compound_elements=True)

    assert np.allclose(np.interp(twxb1.s, twmb1.s, twmb1.mux), twxb1.mux, rtol=0, atol=4e-8)
    assert np.allclose(np.interp(twxb1.s, twmb1.s, twmb1.muy), twxb1.muy, rtol=0, atol=4e-8)
    assert np.allclose(np.interp(twxb2.s, twmb2.s, twmb2.mux), twxb2.mux, rtol=0, atol=4e-8)
    assert np.allclose(np.interp(twxb2.s, twmb2.s, twmb2.muy), twxb2.muy, rtol=0, atol=4e-8)

    for kk in phasechange_test_vals.keys():
        mad.globals[kk] = 0
        collider.vars[kk] = 0