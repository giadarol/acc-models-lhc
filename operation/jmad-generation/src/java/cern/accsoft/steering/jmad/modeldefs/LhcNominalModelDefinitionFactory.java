/**
 *
 */
package cern.accsoft.steering.jmad.modeldefs;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.modeldefs.create.AbstractLhcModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSet;
import cern.accsoft.steering.jmad.modeldefs.create.OpticDefinitionSetBuilder;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;

import java.util.ArrayList;
import java.util.List;

import static cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType.STRENGTHS;
import static cern.accsoft.steering.jmad.modeldefs.create.OpticModelFileBuilder.modelFile;

public class LhcNominalModelDefinitionFactory extends AbstractLhcModelDefinitionFactory {

    @Override
    protected void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
        modelDefinition.addInitFile(new CallableModelFileImpl("toolkit/init-constants.madx"));
        modelDefinition.addInitFile(new CallableModelFileImpl("lhc.seq"));
    }

    @Override
    protected String getModelDefinitionName() {
        return "LHC 2024";
    }

    @Override
    protected List<OpticDefinitionSet> getOpticDefinitionSets() {
        List<OpticDefinitionSet> definitionSetList = new ArrayList<>();

//        /* 2024 VdM optics */
        definitionSetList.add(create2024VdMOpticsSet());

        /* 2024 injection optics with Phase knob 25% */
        definitionSetList.add(createInjectionOptics_25Knob());

        /* 2024 injection optics with Phase knob 50% */
        definitionSetList.add(createInjectionOptics_50Knob());

        /* 2024 injection optics with Phase knob 75% */
        definitionSetList.add(createInjectionOptics_75Knob());

        /* 2024 injection optics with Phase knob 100% */
        definitionSetList.add(createInjectionOptics_100Knob());

        /* 2024 low beta optics */
        definitionSetList.add(createLowBetaOpticsSet());

        /* IONS optics */
        definitionSetList.add(createIR2SqueezeIonsOpticsSet());

        /* IR7 squeeze optics */
        definitionSetList.add(createIR7SqueezeOpticsSet());

        /* Injection optics for PPLP ramp in steps (nonATS knob) */
        definitionSetList.add(createInjectionOpticsWithNonATSKnobs());

        /* Injection HL optics for MD */
        definitionSetList.add(createHLOpticsForMD());

        /* 60deg optics */
        definitionSetList.add(create60degOpticsForMD());

        return definitionSetList;
    }

    /**
     * ATS ramp and squeeze ... All at collision tune --> trimmed with knob to INJ (BP level)
     * @return
     */

    private OpticDefinitionSet createInjectionOptics_25Knob() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));

        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/set-phasechange-knob-25strength.madx"));

        // 2024 optics
        builder.addOptic("R2024aRP_A11mC11mA10mL10m_PhaseKnob25ON", modelFile("strengths/ATS_Nominal/2024/ats_11m.madx"));

        return builder.build();
    }

    private OpticDefinitionSet createInjectionOptics_50Knob() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));

        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/set-phasechange-knob-50strength.madx"));

        // 2024 optics
        builder.addOptic("R2024aRP_A11mC11mA10mL10m_PhaseKnob50ON", modelFile("strengths/ATS_Nominal/2024/ats_11m.madx"));

        return builder.build();
    }
    private OpticDefinitionSet createInjectionOptics_75Knob() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));

        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/set-phasechange-knob-75strength.madx"));

        // 2024 optics
        builder.addOptic("R2024aRP_A11mC11mA10mL10m_PhaseKnob75ON", modelFile("strengths/ATS_Nominal/2024/ats_11m.madx"));

        return builder.build();
    }
    private OpticDefinitionSet createInjectionOptics_100Knob() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/set-phasechange-knob-100strength.madx"));

        // 2024 optics
        builder.addOptic("R2024aRP_A11mC11mA10mL10m_PhaseKnob100ON", modelFile("strengths/ATS_Nominal/2024/ats_11m.madx"));

        return builder.build();
    }

    private OpticDefinitionSet createInjectionOpticsWithNonATSKnobs() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));

        // 2024 injection optics for PPLP ramp in steps (to have non-ATS knob)
        builder.addOptic("R2024aRP_A11mC11mA10mL10m_nonATSKnobs", modelFile("strengths/ATS_Nominal/2024/ats_11m.madx"));

        return builder.build();
    }

    private OpticDefinitionSet createLowBetaOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));        
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-phasechange-knobs.madx"));

        // 2024 optics
        builder.addOptic("R2024aRP_A11mC11mA10mL10m", modelFile("strengths/ATS_Nominal/2024/ats_11m.madx"));
        builder.addOptic("R2024aRP_A10mC10mA10mL10m", modelFile("strengths/ATS_Nominal/2024/ats_10m.madx"));
        builder.addOptic("R2024aRP_A970cmC970cmA10mL970cm", modelFile("strengths/ATS_Nominal/2024/ats_970cm.madx"));
        builder.addOptic("R2024aRP_A930cmC930cmA10mL930cm", modelFile("strengths/ATS_Nominal/2024/ats_930cm.madx"));
        builder.addOptic("R2024aRP_A880cmC880cmA10mL880cm", modelFile("strengths/ATS_Nominal/2024/ats_880cm.madx"));
        builder.addOptic("R2024aRP_A810cmC810cmA10mL810cm", modelFile("strengths/ATS_Nominal/2024/ats_810cm.madx"));
        builder.addOptic("R2024aRP_A700cmC700cmA10mL700cm", modelFile("strengths/ATS_Nominal/2024/ats_700cm.madx"));
        builder.addOptic("R2024aRP_A600cmC600cmA10mL600cm", modelFile("strengths/ATS_Nominal/2024/ats_600cm.madx"));
        builder.addOptic("R2024aRP_A510cmC510cmA10mL510cm", modelFile("strengths/ATS_Nominal/2024/ats_510cm.madx"));
        builder.addOptic("R2024aRP_A440cmC440cmA10mL440cm", modelFile("strengths/ATS_Nominal/2024/ats_440cm.madx"));
        builder.addOptic("R2024aRP_A370cmC370cmA10mL370cm", modelFile("strengths/ATS_Nominal/2024/ats_370cm.madx"));
        builder.addOptic("R2024aRP_A310cmC310cmA10mL310cm", modelFile("strengths/ATS_Nominal/2024/ats_310cm.madx"));
        builder.addOptic("R2024aRP_A250cmC250cmA10mL250cm", modelFile("strengths/ATS_Nominal/2024/ats_250cm.madx"));

        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_1", modelFile("strengths/ATS_Nominal/2024/ats_200cm_1.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-83", modelFile("strengths/ATS_Nominal/2024/ats_200cm_0-83.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-72", modelFile("strengths/ATS_Nominal/2024/ats_200cm_0-75.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-65", modelFile("strengths/ATS_Nominal/2024/ats_200cm_0-66.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-57", modelFile("strengths/ATS_Nominal/2024/ats_200cm_0-57.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5", modelFile("strengths/ATS_Nominal/2024/ats_200cm_0-5.madx"));

        builder.addOptic("R2024aRP_A156cmC156cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_156cm.madx"));
        builder.addOptic("R2024aRP_A120cmC120cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_120cm.madx"));
        builder.addOptic("R2024aRP_A112cmC112cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_112cm.madx"));
        builder.addOptic("R2024aRP_A105cmC105cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_105cm.madx"));
        builder.addOptic("R2024aRP_A99cmC99cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_99cm.madx"));
        builder.addOptic("R2024aRP_A93cmC93cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_93cm.madx"));
        builder.addOptic("R2024aRP_A87cmC87cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_87cm.madx"));
        builder.addOptic("R2024aRP_A82cmC82cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_82cm.madx"));
        builder.addOptic("R2024aRP_A77cmC77cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_77cm.madx"));
        builder.addOptic("R2024aRP_A72cmC72cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_72cm.madx"));
        builder.addOptic("R2024aRP_A68cmC68cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_68cm.madx"));
        builder.addOptic("R2024aRP_A64cmC64cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_64cm.madx"));
        builder.addOptic("R2024aRP_A60cmC60cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_60cm.madx"));
        builder.addOptic("R2024aRP_A56cmC56cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_56cm.madx"));
        builder.addOptic("R2024aRP_A52cmC52cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_52cm.madx"));
        builder.addOptic("R2024aRP_A48cmC48cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_48cm.madx"));
        builder.addOptic("R2024aRP_A45cmC45cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_45cm.madx"));
        builder.addOptic("R2024aRP_A41cmC41cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_41cm.madx"));
        builder.addOptic("R2024aRP_A38cmC38cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_38cm.madx"));
        builder.addOptic("R2024aRP_A35cmC35cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_35cm.madx"));
        builder.addOptic("R2024aRP_A32cmC32cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_32cm.madx"));
        builder.addOptic("R2024aRP_A30cmC30cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_30cm.madx"));
        builder.addOptic("R2024aRP_A28cmC28cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_28cm.madx"));
        builder.addOptic("R2024aRP_A26cmC26cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_26cm.madx"));
        builder.addOptic("R2024aRP_A24cmC24cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_24cm.madx"));
        builder.addOptic("R2024aRP_A22cmC22cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_22cm.madx"));
        builder.addOptic("R2024aRP_A20cmC20cmA10mL200cm", modelFile("strengths/ATS_Nominal/2024/ats_20cm.madx"));

        //Flat optics for 2025 MD
        builder.addOptic("R2024aRP_A60_54cmC60_54cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_54cm.madx"));
        builder.addOptic("R2024aRP_A60_49cmC60_49cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_49cm.madx"));
        builder.addOptic("R2024aRP_A60_44cmC60_44cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_44cm.madx"));
        builder.addOptic("R2024aRP_A60_40cmC60_40cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_40cm.madx"));
        builder.addOptic("R2024aRP_A60_36cmC60_36cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_36cm.madx"));
        builder.addOptic("R2024aRP_A60_33cmC60_33cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_33cm.madx"));
        builder.addOptic("R2024aRP_A60_30cmC60_30cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_30cm.madx"));
        builder.addOptic("R2024aRP_A60_27cmC60_27cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_27cm.madx"));
        builder.addOptic("R2024aRP_A60_25cmC60_25cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_25cm.madx"));
        builder.addOptic("R2024aRP_A60_22cmC60_22cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_22cm.madx"));
        builder.addOptic("R2024aRP_A60_20cmC60_20cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_20cm.madx"));
        builder.addOptic("R2024aRP_A60_18cmC60_18cmA10mL200cm_MD", modelFile("strengths/ATS_Nominal/2024_MD/60_18cm.madx"));

        return builder.build();
    }

    private OpticDefinitionSet create2024VdMOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-non-ats.madx"));

        // VdM optics
        builder.addOptic("R2024hRP_A11_2mC11_2mA11_2mL11_2m", modelFile("strengths/ATS_Nominal/VdM_2024/11_2m.madx"));
        builder.addOptic("R2024hRP_A12_6mC12_6mA12_6mL12_6m", modelFile("strengths/ATS_Nominal/VdM_2024/12_6m.madx"));
        builder.addOptic("R2024hRP_A14_2mC14_2mA14_2mL14_2m", modelFile("strengths/ATS_Nominal/VdM_2024/14_2m.madx"));
        builder.addOptic("R2024hRP_A16mC16mA16mL16m", modelFile("strengths/ATS_Nominal/VdM_2024/16m.madx"));
        builder.addOptic("R2024hRP_A17_8mC17_8mA17_8mL17_8m", modelFile("strengths/ATS_Nominal/VdM_2024/17_8m.madx"));
        builder.addOptic("R2024hRP_A19_2mC19_2mA19_2mL19_8m", modelFile("strengths/ATS_Nominal/VdM_2024/19_2m.madx"));
        builder.addOptic("R2024hRP_A19_2mC19_2mA19_2mL24m", modelFile("strengths/ATS_Nominal/VdM_2024/19_2m_IP8.madx"));

        return builder.build();
    }

    private OpticDefinitionSet createIR7SqueezeOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));

        // IR7 squeeze optics
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5_IR7_1", modelFile("strengths/ATS_Nominal/IR7_Squeeze_MD/2024/ats200cmIR7_1.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5_IR7_2", modelFile("strengths/ATS_Nominal/IR7_Squeeze_MD/2024/ats200cmIR7_2.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5_IR7_3", modelFile("strengths/ATS_Nominal/IR7_Squeeze_MD/2024/ats200cmIR7_3.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5_IR7_4", modelFile("strengths/ATS_Nominal/IR7_Squeeze_MD/2024/ats200cmIR7_4.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5_IR7_5", modelFile("strengths/ATS_Nominal/IR7_Squeeze_MD/2024/ats200cmIR7_5.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5_IR7_6", modelFile("strengths/ATS_Nominal/IR7_Squeeze_MD/2024/ats200cmIR7_6.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5_IR7_7", modelFile("strengths/ATS_Nominal/IR7_Squeeze_MD/2024/ats200cmIR7_7.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5_IR7_8", modelFile("strengths/ATS_Nominal/IR7_Squeeze_MD/2024/ats200cmIR7_8.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5_IR7_9", modelFile("strengths/ATS_Nominal/IR7_Squeeze_MD/2024/ats200cmIR7_9.madx"));
        builder.addOptic("R2024aRP_A200cmC200cmA10mL200cm_0-5_IR7_10", modelFile("strengths/ATS_Nominal/IR7_Squeeze_MD/2024/ats200cmIR7_10.madx"));

        return builder.build();
    }


    private OpticDefinitionSet createIR2SqueezeIonsOpticsSet() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-non-ats.madx"));
        
        // ION optics
        builder.addOptic("R2024iRP_A11mC11mA10mL10m", modelFile("strengths/ATS_Nominal/2024_IONS/11_10m.madx"));
        builder.addOptic("R2024iRP_A970cmC970cmA970cmL970cm", modelFile("strengths/ATS_Nominal/2024_IONS/970cm.madx"));
        builder.addOptic("R2024iRP_A920cmC920cmA920cmL920cm", modelFile("strengths/ATS_Nominal/2024_IONS/920cm.madx"));
        builder.addOptic("R2024iRP_A850cmC850cmA850cmL850cm", modelFile("strengths/ATS_Nominal/2024_IONS/850cm.madx"));
        builder.addOptic("R2024iRP_A760cmC760cmA760cmL760cm", modelFile("strengths/ATS_Nominal/2024_IONS/760cm.madx"));
        builder.addOptic("R2024iRP_A670cmC670cmA670cmL670cm", modelFile("strengths/ATS_Nominal/2024_IONS/670cm.madx"));
        builder.addOptic("R2024iRP_A590cmC590cmA590cmL590cm", modelFile("strengths/ATS_Nominal/2024_IONS/590cm.madx"));
        builder.addOptic("R2024iRP_A520cmC520cmA520cmL520cm", modelFile("strengths/ATS_Nominal/2024_IONS/520cm.madx"));
        builder.addOptic("R2024iRP_A450cmC450cmA450cmL450cm", modelFile("strengths/ATS_Nominal/2024_IONS/450cm.madx"));
        builder.addOptic("R2024iRP_A400cmC400cmA400cmL400cm", modelFile("strengths/ATS_Nominal/2024_IONS/400cm.madx"));
        builder.addOptic("R2024iRP_A360cmC360cmA360cmL360cm", modelFile("strengths/ATS_Nominal/2024_IONS/360cm.madx"));
        builder.addOptic("R2024iRP_A320cmC320cmA320cmL320cm", modelFile("strengths/ATS_Nominal/2024_IONS/320cm.madx"));
        builder.addOptic("R2024iRP_A290cmC290cmA290cmL290cm", modelFile("strengths/ATS_Nominal/2024_IONS/290cm.madx"));
        builder.addOptic("R2024iRP_A230cmC230cmA230cmL230cm", modelFile("strengths/ATS_Nominal/2024_IONS/230cm.madx"));
        builder.addOptic("R2024iRP_A185cmC185cmA185cmL185cm", modelFile("strengths/ATS_Nominal/2024_IONS/185cm.madx"));
        builder.addOptic("R2024iRP_A135cmC135cmA135cmL150cm", modelFile("strengths/ATS_Nominal/2024_IONS/135cm.madx"));
        builder.addOptic("R2024iRP_A100cmC100cmA100cmL150cm", modelFile("strengths/ATS_Nominal/2024_IONS/100cm.madx"));
        builder.addOptic("R2024iRP_A82cmC82cmA82cmL150cm", modelFile("strengths/ATS_Nominal/2024_IONS/82cm.madx"));
        builder.addOptic("R2024iRP_A68cmC68cmA68cmL150cm", modelFile("strengths/ATS_Nominal/2024_IONS/68cm.madx"));
        builder.addOptic("R2024iRP_A57cmC57cmA57cmL150cm", modelFile("strengths/ATS_Nominal/2024_IONS/57cm.madx"));
        builder.addOptic("R2024iRP_A50cmC50cmA50cmL150cm", modelFile("strengths/ATS_Nominal/2024_IONS/50cm.madx"));
    
        return builder.build();
    }

    private OpticDefinitionSet createHLOpticsForMD() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
//        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
//        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-ats.madx"));
//        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-ats.madx"));
//        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));

        // HL-LHC optics for MD
        builder.addOptic("R2024HL_A11mC11mA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_0.madx"));
        builder.addOptic("R2024HL_A10mC10mA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_1.madx"));
        builder.addOptic("R2024HL_A930cmC930cmA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_2.madx"));
        builder.addOptic("R2024HL_A845cmC845cmA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_3.madx"));
        builder.addOptic("R2024HL_A760cmC760cmA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_4.madx"));
        builder.addOptic("R2024HL_A675cmC675cmA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_5.madx"));
        builder.addOptic("R2024HL_A590cmC590cmA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_6.madx"));
        builder.addOptic("R2024HL_A505cmC505cmA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_7.madx"));
        builder.addOptic("R2024HL_A420cmC420cmA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_8.madx"));
        builder.addOptic("R2024HL_A335cmC335cmA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_9.madx"));
        builder.addOptic("R2024HL_A250cmC250cmA10mL10m_MD", modelFile("strengths/md_hllhc/v2/ramp_10.madx"));

        builder.addOptic("R2024HL_A250cmC250cmA10mL10m_MD_prova", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_0.madx"));
        builder.addOptic("R2024HL_A230_195cmC230_195cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_1.madx"));
        builder.addOptic("R2024HL_A200_135cmC200_135cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_3.madx"));
        builder.addOptic("R2024HL_A180_100cmC180_100cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_5.madx"));
        builder.addOptic("R2024HL_A165_84cmC165_84cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_7.madx"));
        builder.addOptic("R2024HL_A150_135cmC150_135cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_9.madx"));
        builder.addOptic("R2024HL_A145_65cmC145_65cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_10.madx"));
        builder.addOptic("R2024HL_A135_60cmC135_60cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_11.madx"));
        builder.addOptic("R2024HL_A130_57cmC130_57cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_12.madx"));
        builder.addOptic("R2024HL_A125_55cmC125_55cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_13.madx"));
        builder.addOptic("R2024HL_A120_50cmC120_50cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_14.madx"));
        builder.addOptic("R2024HL_A120_48cmC120_48cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_15.madx"));
        builder.addOptic("R2024HL_A115_45cmC115_45cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_16.madx"));
        builder.addOptic("R2024HL_A110_43cmC110_43cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_17.madx"));
        builder.addOptic("R2024HL_A105_39cmC105_39cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_19.madx"));
        builder.addOptic("R2024HL_A100_37cmC100_37cmA10mL10m_MD", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_20.madx"));
        // New optics with fix on Q8L2, after too large unbalance during MD#3. Optics 18 was never imported (mistake?), so I keep not importing it
        builder.addOptic("R2024HL_A110_43cmC110_43cmA10mL10m_MD_fixed", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_17_fix.madx"));
        builder.addOptic("R2024HL_A105_39cmC105_39cmA10mL10m_MD_fixed", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_19_fix.madx"));
        builder.addOptic("R2024HL_A100_37cmC100_37cmA10mL10m_MD_fixed", modelFile("strengths/md_hllhc/sqvh_v0/squeeze_20_fix.madx"));

        return builder.build();
    }

    private OpticDefinitionSet create60degOpticsForMD() {
        OpticDefinitionSetBuilder builder = OpticDefinitionSetBuilder.newInstance();

        /* initial optics strength files common to all optics (loaded before the other strength files) */
        builder.addInitialCommonOpticFile(modelFile("toolkit/zero-strengths.madx"));
        /* final optics strength files common to all optics (loaded after the other strength files) */
        builder.addFinalCommonOpticFile(modelFile("toolkit/reset-bump-flags.madx").parseAs(STRENGTHS));
        builder.addFinalCommonOpticFile(modelFile("toolkit/match-lumiknobs.madx"));
        /* Define correct knobs to be used operationally */
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-tune-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-chroma-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-op-coupling-knobs-non-ats.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b1.madx"));
        builder.addFinalCommonOpticFile(modelFile("toolkit/generate-dptrim-knob-b2.madx"));

        // HL-LHC optics for MD
        builder.addOptic("R2024aRP_A11mC11mA10mL10m_60deg", modelFile("strengths/60deg/60deg2024.madx"));

        return builder.build();
    }


}